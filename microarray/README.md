# Micorarray Analysis #


### Annotation of the code ###

* 'src' folder contains all the scripts used for microarray analysis
* You need to creat a data folder here for all .CEL files
* dependencies.yaml includes all the packages used in the analysis
* copy the whole microarray directory to your local server and type make to run all the scripts in 'src'

### Author ###

* Zhang Siting
