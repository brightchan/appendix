results.dir <- "results"
data.dir <- "data"

.today <- as.Date(Sys.time())

results.dir <- paste(results.dir,.today,sep="/")

dir.create(results.dir)

resultsfile <- function(file) {
    paste(results.dir,file,sep="/")
}
