#!/bin/bash

#####################################################
######## Merge two bam files of the same sample######
#####################################################

bam_dir=/mnt/gtklab01/siting/CHIPseq/bamF2

cd ${bam_dir}

Lane1_files=($(ls Lane1/*.bam))
Lane2_files=($(ls Lane2/*.bam))

for ((i=0;i<${#Lane1_files[@]};i++));
do
    M_bam=${prefix2}.bam
    echo 'Merging bam files on' ${Lane1_files[i]} '&' ${Lane2_files[i]} '@' $(date)
    samtools merge ${M_bam} ${Lane1_files[i]} ${Lane2_files[i]}
    echo 'Done @ ' $(date)
done

##########################################
############## Bed files transfermation & Extend reads to 200 bp
###get genome sizes
mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e "select chrom, size from hg19.chromInfo"  > hg19.genome

bam_array=($(ls *.bam))
for bam in "${bam_array[@]}"
do
   bamToBed -i ${bam} > bedFiles/${bam#*/}.bed
   bedtools slop -l 0 -r 150 -s -g hg19.genome \
	-i bedFiles/${bam#*/}.bed > bedFiles/${bam#*/}_extendedreads.bed
done
