#!bin/bash

#####################################
###### Call peaks with MACS2 ########
#####################################

bamdir=/path/to/bamFiles
beddir=$topdir/bedFiles
macdir=$topdir/macs2_results

if [ ! -d "$macdir" ]; then
    echo "Creating MACS2 peak calling folder"
    mkdir -p $macdir
    'Done'
fi

cd ${beddir}
chip_array=($(ls *_ChIP_*.bed))
input_array=($(ls *_Input*_.bed))

a_len=${#chip_array[@]}
for (( i=0; i<${a_len}; i++ )); 
do
    echo 'Calling peaks on' ${chip_array[i]} 'vs' ${input_array[i]} '@' $(date)
    macs2 callpeak -t ${chip_array[i]} -c ${input_array[i]} --outdir ${macdir} --call-summits -n ${chip_array[i]%.*}
    Rscript ${macdir}/ ${chip_array[i]%.*}.r
    echo 'Done @' $(date)
done
