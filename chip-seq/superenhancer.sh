#!/bin/bash

############################################
####### Call enhancers with HOMER ##############
############################################

topdir=/path/to/top
bamdir=/path/to/bam
tagdir=$topdir/homer_tags
peakdir=$topdir/homer_peaks
enhancerdir=$topdir/homer_peakresult/enhancer

cd ${bamdir}
chip_array=($(ls *_ChIP*.bam))
input_array=($(ls *_Input*.bam))

a_len=${#chip_array[@]}
for (( i=0; i<${a_len}; i++ ));
do
    findPeaks $tagdir/${chip_array[i]%.*} -style super -o $enhancerdir/${chip_array[i]%.*}_superenhancer.txt -i $tagdir/${input_array[i]%.*} -typical $enhancerdir/${chip_array[i]%.*}_typicalenhancer.txt
    echo 'Done @' $(date)
done
