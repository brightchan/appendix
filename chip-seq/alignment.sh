#!/bin/bash

#######################################
#### Mapping reads to the genome#######
#######################################


topdir=/path/to/top
indexDir=$topdir/UCSChg19/hg19
fqdir=$topdir/fastq

cd ${fqdir}
###############################
##fastq.gz files
## perform unzip first if necessary
# gz_array=($(ls *.fastq.gz))
# a_len=${#gz_array[@]}
# for (( i=0; i<${a_len}; i++ ));
# do 
#     output="${gz_array[$i]%.gz}"
#     gunzip -c ${gz_array[$i]} > ${output}
# done

fq_array=($(ls *.fastq))

## perform the alignment here 
## defines the alignment variables
nflag=4
genome=hg19
readlength=50
nthreads=10

function performAlignment(){
    local sample=$1
    local prefix="${sample%.*}"
    local insam=${prefix}.i.sam
    local fileSAM=${prefix}.sam
    local fileUnmappedSAM=${prefix}UM.sam
    local tempbam=${fileSAM}_temp.bam
    bowtie2 -p ${nthreads} -x ${indexDir} ${sample} > ${insam}
    map-bowtie2.pl -p ${nthreads} -x ${indexDir} ${sample} > ${insam}
    awk -v awkFlag=${nFlag} -v awkHeader=1 -v awkOutputFileNotT=${fileSAM} -v awkOutputFileNotF=${fileUnmappedSAM} -f filterNegFlagSAM2.awk ${insam}
    samtools view -bhS -F 4 ${fileSAM} > ${tempbam}
    samtools sort ${tempbam} ${prefix}
    samtools index ${prefix}.bam
    rm ${tempbam}
}


############# perform alignment 
for files in "${fq_array[@]}"
do
    echo 'Performing alignment on' ${files} '@' $(date)
    performAlignment ${files}
    echo 'Done @ ' $(date)
done



