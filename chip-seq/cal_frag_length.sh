#!/bin/bash

####################################
### Calculate fragment length#######
####################################

### bigWig -> WIG format
mascdir=/path/to/MaSC
beddir=/path/to/bedfile

### Fetch Chromosome sizes info
cd ${mascdir}
mysql --user=genome --host=genome-mysql.cse.ucsc.edu -A -e "select chrom, size from hg19.chromInfo"  > hg19.chrom.sizes

bed_array=($(ls *.bed))
for sample in ${bed_array[@]};
do
    MaSC.pl -v --mappability_path=$mascdir/wg_map --chrom_length=hg19.chrom.sizes --input_bed=${sample} --prefix=$mascdir/results/${sample%.*}
done
