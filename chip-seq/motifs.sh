#!bin/bash

######################################################
############# Motif analysis with HOMER ##############
######################################################

peakdir=/media/zst/F0A02BE0A02BAC52/ChIP-seq_2016/bamF2
resultdir=$peakdir/motif

cd ${peakdir}
peak_array=($(ls *.txt))
for (( i=0; i<${#peak_array[@]}; i++ ))
do
   echo 'Finding motifs on' ${peak_array[i]} '@' $(date)
   findMotifsGenome.pl ${peak_array[i]} hg19 $resultdir/${peak_array[i]%.*}_motif -size 200 -mask -p 5 -olen 3 -gc
    echo 'Done @' $(date)
done
