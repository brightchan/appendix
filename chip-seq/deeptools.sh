#!/bin/bash

#######################################################
##### Analysis with deeptools to visualize data #######
#######################################################

topdir=/path/to/top
bamdir=$topdir/bamFiles
resultdir=$topdir/deeptoolfiles
figdir=$resultdir/figs

cd ${bamdir}
array=($ls *.bam)
len=${#array[@]}

############### convert bam files to bigWig files if necessary
# for (( i=0; i<${len}; i++ ));
# do
#    echo 'Convert' ${array[i]} 'to bigWig files @' $(date)
# #   samtools index ${array[i]}
#    bamCoverage --bam ${array[i]} \
#        --outFileName $resultdir/${array[i]%.*}.bigWig \
#        --outFileFormat bigwig \
#        --extendReads 150 \
#        --ignoreDuplicates \
#        --scaleFactor 1.2
#    echo 'Done @' $(date)
# done

cd ${resultdir}
matrix_array=($(ls matrix*.gz))
chip_array=($(ls *_ChIP*.bam))
input_array=($(ls *_Input*.bam))
a_len=${#matrix_array[@]}

for (( i=0; i<${a_len}; i++ ));
do
   echo 'Compute matrix on' ${chip_array[i]%.*} '@' $(date)
   computeMatrix reference-point --referencePoint TSS -a 3000 -b 3000 -R $resultdir/sample.bed -S $resultdir/${chip_array[i]%.*}.bigWig --skipZeros -o $resultdir/matrix1_${chip_array[i]%.*}_TSS.gz --outFileSortedRegions $resultdir/regions1_${chip_array[i]%.*}.bed
   echo 'Plot heatmap'
    plotHeatmap -m ${matrix_array[i]} -out $figdir/${matrix_array[i]%.*}_Heatmap1_2.pdf --colorMap YlOrRd --whatToShow 'heatmap and colorbar' --regionsLabel 'Peaks'
   echo 'Plot profile'
   plotProfile -m matrix_TSS.gz -out $figdir/profiles.pdf --perGroup
   echo 'Done @' $(date)
done
