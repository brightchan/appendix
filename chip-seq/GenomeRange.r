library(dplyr)
library(GenomicRanges)

# setwd("/path/to/peakresult")

makeGRanges <- function(peafile){
  d <- peakfile[!grepl("chrUn_gl" , peakfile$V2),]
  d_n <- d[!grepl("random" , d$chr),]
  r <- GRanges(seqnames = d_n$chr,
               ranges = IRanges(start=d_n$start,
                                end=d_n$end, names = d_n$ID),
               strand = d_n$strand,
               score = d_n$score,
               fc= d_n$foldChange)
}

makePeakfile <- function(append){
  df <- data.frame(peakID=elementMetadata(gr)$names,
                   chr=seqnames(append),
                   starts=start(append)-1,
                   ends=end(append),
                   scores=elementMetadata(gr)$score,
                   strands=strand(append))
  write.table(df, file=paste(append,".txt", sep = ""), quote=F, sep="\t", row.names=F)
}

data.frame1 <- read.table("peaks.txt", sep = '\t', stringsAsFactors=F)%>%
 makeGRanges

data.frame2 <- read.table("peaks.txt", sep = '\t', stringsAsFactors=F)%>%
 makeGRanges
 
df_n <- setdiff(data.frame1, data.frame.2)
Diff <- as.data.frame(intersect(df_n, data.frame.2))
write.table(Diff, "Diff.txt", quote=F, sep="\t", row.names=T, col.names = F)

