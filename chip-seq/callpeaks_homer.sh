#!/bin/bash
###########################################################
####### Call peaks and annotation with HOMER ##############
###########################################################

topdir=/path/to/top
bamdir=/path/to/bam
resultdir=$topdir/results
tagdir=$topdir/homer_tags
peakdir=$topdir/homer_peaks
motifdir=$hisdir/motif

if [ ! -d "$dir" ]; then
    echo "Creating the directory needed"
    mkdir -p $dir
    'Done'
fi



cd ${bamdir}
chip_array=($(ls *_ChIP*.bam))
input_array=($(ls *_Input*.bam))

########### Call peaks
a_len=${#chip_array[@]}
for (( i=0; i<${a_len}; i++ ));
do
    echo 'Calling peaks on' ${chip_array[i]} 'vs' ${input_array[i]} '@' $(date)
    findPeaks $tagdir/${chip_array[i]%.*} -style factor -o $resultdir/${chip_array[i]%.*}_peaks.txt -i $tagdir/${input_array[i]%.*}
    echo 'Done @' $(date)
done

########### Peak to Bed files
peak_array=($(ls ${resultdir}/*.txt))
for (( i=0; i<${#peak_array[@]}; i++ ))
do
    pos2bed.pl ${peak_array[i]} > ${peak_array[i]%.*}.bed
done

########### Annotatate peaks
for (( i=0; i<${#peak_array[@]}; i++ ))
do
    echo 'Annotatting peaks on' ${peak_array[i]} 'with' ${chip_array[i]} 'vs' ${input_array[i]} '@' $(date)
      annotatePeaks.pl ${sample[i]} hg19 -go $resultdir/${sample[i]%.*}_GO -genomeOntology $resultdir/${sample[i]%.*}_Genome -annStats $resultdir/${sample[i]%.*}_stats.xlsx > $resultdir/${sample[i]%.*}.xlsx
    echo 'Done @' $(date)
done
