library(plyr)
require(stats)
library(dplyr)


datadir <- "/path]/to/TCGA"
rdir <- "/path/to/mirResult"
gdir <- "path/to/gene"
ndir <- "/path/to/normalize"

dir.create(ndir)
cancer <- list.files(datadir)

########## miRNA
miR <- c("hsa-mir-xxx","hsa-mir-yyy")
for (ca in cancer){
  setwd(paste(datadir, ca, sep="/"))
  if(("nontumor" %in% list.files())==T){
    for (mir in miR){
      if((paste(ca, "_tumor_", mir, ".txt", sep="") %in% list.files(rdir))==T){
        da <- read.table(file=paste(rdir, ca, "_tumor_", mir, ".txt", sep=""), sep="\t", header=T)
        d <- read.table(file=paste(rdir,ca, "_nontumor_",mir, ".txt", sep=""), sep="\t", header=T)
        common <- intersect(da$filenames, d$filenames)
        dc <- da[da$filenames %in% common,]
        dd <- d[d$filenames %in% common,]
        d <- merge(dc,dd,by="filenames")
        d$normalised <- d$mirX.x/d$mirX.y
        write.table(d,paste(ndir,ca,"_",mir,"_normalise.txt",sep=""), sep="\t")
      }
    }
  }
}


############ GENE

for (ca in cancer){
  setwd(paste(datadir, ca, sep="/"))
  if("nontumor" %in% list.files()==T){
    da <- read.table(file=paste(gdir, ca, "_tumor_gene_barcode.txt", sep=""), sep="\t", header=T)
    d <- read.table(file=paste(gdir,ca, "_nontumor_gene_barcode.txt", sep=""), sep="\t", header=T)
    common <- intersect(da$patient_barcode, d$patient_barcode)
    dc <- da[da$patient_barcode %in% common,]
    dd <- d[d$patient_barcode %in% common,]
    d <- merge(dc,dd,by="patient_barcode")
    d$normalised <- d$normalised_gene.x/d$normalised_gene.y
    write.table(d, paste(ndir,ca,"_gene_normalise.txt",sep=""), sep="\t")
  }
}
