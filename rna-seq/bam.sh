#!bin/bash

topdir=/mnt/gtklab01/siting/tham/star_out

cd ${topdir}
name_array=($(ls -d -- *))
for files in "${name_array[@]}"
do
    cp ${files}/${files%/*}Aligned.sortedByCoord.out.bam /mnt/gtklab01/siting/tham/deseq2/
done
