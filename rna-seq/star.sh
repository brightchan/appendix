#!/bin/bash

###############################################################
################# Perform alignments with STAR ################
###############################################################
echo  "**********Runing STAR with Homemade Bash Script********"

top=/path/to/top
data=/path/to/data
#outp=/mnt/gtklab01/siting/tham/star_out/"$1"_star
indexDir=/path/to/STAR_ind
gtf=/path/to/Homo_sapiens.GRCh38.84.gtf
genome=$top/Homo_sapiens.GRCh38.dna_sm.primary_assembly.fa


if [ ! -d "$indexDir" ]; then
    echo "Creating Index Directory Folder"
    mkdir -p $indexDir
    echo 'Done'
fi

function star_align(){
    local name=$1
    local data=/path/to/data
    local prefix="${name%_*}"
    local reads=$data/${prefix}_1.fastq.gz
    local reads2=$data/${prefix}_2.fastq.gz
    local indexDir=$indexDir
    local gtf=$gtf
    local genome=$genome
    local outp=/path/to/star_out/${prefix}_star/${prefix}_star

    if [ ! -d "$outp" ]; then
    echo "Creating Output Directory Folder"
    mkdir -p $outp
    echo 'Done'
    fi
    STAR --genomeDir $indexDir --runThreadN 25 --sjdbGTFfile $gtf -genomeLoad LoadAndKeep --outSAMtype BAM SortedByCoordinate --readFilesIn $reads $reads2 --readFilesCommand zcat --outFilterMultimapNmax 1 --outFileNamePrefix $outp
} #### 25 threads were used in this case, reset this value according to the server

#STAR index generation:
#    STAR --runThreadN 25 --runMode genomeGenerate --genomeDir $indexDir --genomeFastaFiles $genome --sjdbGTFfile $gtf --sjdbOverhang 100
#STAR batch run:
   cd $data
#    ls *_1.fastq.gz | xargs -n 1 -P 1 ./star.sh #list contains SRA id
#    ls *_1.fastq.gz | xargs -n 1 -P 1 star_align

##################################### Running STAR
fastq_array=($(ls *.fastq.gz))
for files in "${fastq_array[@]}"
do
    echo 'Star running STAR and performing alignment on' ${files} '@' $(date)
    star_align ${files}
    echo 'Done @ ' $(date)
done
